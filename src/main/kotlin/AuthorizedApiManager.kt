
interface AuthorizedApiManager {
    fun getAuthorizationLink(userId: Long): String
    fun authorizeUser(stateCode: String, code: String): Boolean
    fun getAuthorizedApi(userId: Long): SpotifyApi?
}