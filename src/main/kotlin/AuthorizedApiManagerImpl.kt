import com.wrapper.spotify.SpotifyApi as SpotifyApiLib
import com.wrapper.spotify.SpotifyHttpManager
import org.slf4j.LoggerFactory
import java.time.Instant
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.HashMap
import kotlin.concurrent.withLock

class AuthorizedApiManagerImpl(
    private val clientId: String,
    private val clientSecret: String,
    private val redirectUrl: String,
    private val userCredentialsDao: UserCredentialsDao
) : AuthorizedApiManager {
    private val lock = ReentrantLock()
    private val stateCodes = ConcurrentHashMap<String, Long>()
    private val stateCodesByUser = HashMap<Long, String>()
    private val logger = LoggerFactory.getLogger(AuthorizedApiManagerImpl::class.java)

    private val spotify = buildSpotifyApi()

    override fun getAuthorizationLink(userId: Long): String {
        val stateCode = lock.withLock {
            val code = stateCodesByUser[userId]
            if (code != null)
                return@withLock code

            val newCode = UUID.randomUUID().toString()
            stateCodesByUser[userId] = newCode
            stateCodes[newCode] = userId
            newCode
        }

        return spotify.authorizationCodeUri()
            .scope("playlist-modify-public")
            .state(stateCode)
            .build()
            .uri.toString()
    }

    override fun authorizeUser(stateCode: String, code: String): Boolean {
        if (!stateCodes.containsKey(stateCode)) return false

        val userId = lock.withLock {
            val userId = stateCodes.remove(stateCode)
            userId?.let { stateCodesByUser.remove(it) }
            userId
        } ?: return false

        val req = spotify.authorizationCode(code).build()
        return try {
            val creds = req.execute()
            userCredentialsDao.store(
                UserCredentialsDao.UserCredentials(
                    userId = userId,
                    accessToken = creds.accessToken,
                    refreshToken = creds.refreshToken,
                    expiresOn = Instant.now().plusSeconds(creds.expiresIn.toLong())
                )
            )
            true
        } catch (e: Throwable) {
            logger.error("authorizeUser", e)
            throw e
        }
    }

    override fun getAuthorizedApi(userId: Long): SpotifyApi? {
        val creds = userCredentialsDao.getUserCreds(userId) ?: return null

        val api = buildSpotifyApi(creds.accessToken, creds.refreshToken)

        val tokenUpdater = object : TokenInfoUpdater {
            override fun newToken(
                accesToken: String,
                refreshToken: String,
                expiresOn: Instant
            ): SpotifyApiLib {
                userCredentialsDao.store(
                    UserCredentialsDao.UserCredentials(
                        creds.userId,
                        accesToken,
                        refreshToken,
                        expiresOn
                    )
                )

                return buildSpotifyApi(accesToken, refreshToken)
            }
        }

        return SpotifyApi(api, tokenUpdater)
    }

    private fun buildSpotifyApi() = SpotifyApiLib.Builder()
        .setClientId(clientId)
        .setClientSecret(clientSecret)
        .setRedirectUri(SpotifyHttpManager.makeUri(redirectUrl))
        .build()

    private fun buildSpotifyApi(accessToken: String, refreshToken: String) = SpotifyApiLib.Builder()
        .setClientId(clientId)
        .setClientSecret(clientSecret)
        .setAccessToken(accessToken)
        .setRefreshToken(refreshToken)
        .setRedirectUri(SpotifyHttpManager.makeUri(redirectUrl))
        .build()
}