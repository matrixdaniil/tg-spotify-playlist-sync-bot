import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.text
import com.github.kotlintelegrambot.entities.ParseMode
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import mu.KotlinLogging
import okhttp3.logging.HttpLoggingInterceptor
import java.sql.DriverManager
import java.util.*
import java.util.concurrent.ConcurrentHashMap

fun main(args: Array<String>) {
    val cfg = Config(args[0])

    val dbConnection = DriverManager.getConnection("jdbc:sqlite:${cfg.dbPath}")
    val credsDao = UserCredentialsDao(dbConnection)
    val chatPlaylistDao = ChatPlaylistDao(dbConnection)
    val playlistInfoDao = PlaylistInfoDao(dbConnection)
    val userInfoDao = UserInfoDao(dbConnection)
    val apiManager = AuthorizedApiManagerImpl(
        clientId = cfg.spotifyClientId,
        clientSecret = cfg.spotifySecret,
        redirectUrl = cfg.redirectUrl,
        credsDao
    );
    val userGroupRequests = ConcurrentHashMap<Long, Long>()
    val timer = Timer()
    val logger = KotlinLogging.logger("main")

    logger.warn("hi")

    val httpServer = embeddedServer(Netty, port = cfg.httpServerPort) {
        routing {
            get("/redirect") {
                val code = call.request.queryParameters["code"]!!
                val state = call.request.queryParameters["state"]!!
                try {
                    if (apiManager.authorizeUser(state, code))
                        call.respond("Now you can use the bot")
                    else
                        call.respond(
                            HttpStatusCode.NotFound, "Try to call /authorize one more time." +
                                    "Your request is invalid. No such state key."
                        )
                } catch (e: Throwable) {
                    call.respond(
                        HttpStatusCode.InternalServerError, "Try to call /authorize one more time. " +
                                "There is something wrong."
                    )
                }
            }
        }
    }
    httpServer.start(wait = false)

    val tgBot = bot {
        token = cfg.tgToken
        logLevel = HttpLoggingInterceptor.Level.NONE
        dispatch {
            command("start") { bot, update ->
                bot.sendMessage(chatId = update.message!!.chat.id, text = "Hi there!")
            }
            command("authorize") { bot, update ->
                try {
                    val userId = update.message!!.from!!.id
                    val url = apiManager.getAuthorizationLink(userId)
                    bot.sendMessage(
                        chatId = update.message!!.chat.id, text = "Follow the link to allow bot modifying " +
                                "and watching for playlist $url"
                    )
                } catch (e: Throwable) {
                    logger.error("/auth", e)
                }
            }
            command("playlist") { bot, update ->
                try {
                    val chat = update.message!!.chat

                    if (update.message!!.from!!.isBot)
                        return@command

                    if (chat.type != "group" && chat.type != "supergroup") {
                        bot.sendMessage(chatId = chat.id, text = "This bot works only in groups (and suppergroups).")
                        return@command
                    }

                    val alreadyLinked = chatPlaylistDao.getPlaylist(chat.id)
                    if (alreadyLinked != null) {
                        bot.sendMessage(
                            chatId = chat.id,
                            text = "User tg://user?id=${alreadyLinked.addedBy} already linked a playlist. You better get in touch with him."
                        )
                        return@command
                    }

                    val userId = update.message!!.from!!.id
                    userGroupRequests[userId] = chat.id
                    bot.sendMessage(chatId = chat.id, text = "PM me a playlist you want to link with that group")
                } catch (e: Throwable) {
                    logger.error("/playlist", e)
                }
            }
            command("ping") { bot, update ->
                bot.sendMessage(chatId = update.message!!.chat.id, text = "pong")
            }
            text() { bot, update ->
                try {
                    if (update.message!!.text!!.startsWith("/"))
                        return@text

                    val userId = update.message!!.from!!.id
                    val chatId = userGroupRequests[userId] ?: return@text

                    val playlistRe = Regex("^https?://open\\.spotify\\.com/playlist/([a-zA-Z0-9\\-_]+).*$")
                    val reResult = playlistRe.find(update.message!!.text!!)
                    val playlist = reResult?.groups?.get(1)?.value
                    if (playlist == null) {
                        bot.sendMessage(chatId = update.message!!.chat.id, text = "Just send a valid link to playlist")
                        return@text
                    }

                    val api = apiManager.getAuthorizedApi(userId)

                    if (api == null) {
                        bot.sendMessage(
                            chatId = update.message!!.chat.id,
                            text = "There is no session linked with your telegram account. " +
                                    "Use /authorize to be able to use this bot"
                        )
                        return@text
                    }

                    try {
                        PlaylistScanner(playlistInfoDao, api, playlist).scanEnd()
                    } catch (e: Throwable) {
                        bot.sendMessage(
                            chatId = update.message!!.chat.id,
                            text = "Probably you don't have permissions to that playlist"
                        )
                    }
                    chatPlaylistDao.link(chatId, playlist, update.message!!.from!!.id)
                    bot.sendMessage(
                        chatId = update.message!!.chat.id,
                        text = "Ok. Any music from group now will be added to playlist"
                    )
                } catch (e: Throwable) {
                    logger.error("text", e)
                }
            }
        }
    }

    val timerTask = object : TimerTask() {
        override fun run() {
            try {
                val playlists = chatPlaylistDao.getAllPlaylists()
                val iterator = playlists.iterator()
                while (iterator.hasNext()) {
                    val playlist = iterator.next()
                    val api = apiManager.getAuthorizedApi(playlist.addedBy) ?: continue
                    val newItems = PlaylistScanner(playlistInfoDao, api, playlist.playlistId).scan()
                    printItems(
                        newItems,
                        playlist.chatId,
                        "https://open.spotify.com/playlist/${playlist.playlistId}",
                        api
                    )
                }
            } catch (e: Throwable) {
                logger.error("timerTask", e)
            }
        }

        private fun printItems(newItems: List<PlaylistItemDto>, chatId: Long, playlistUrl: String, api: SpotifyApi) {
            if (newItems.isEmpty())
                return

            val tracksByUsers = newItems.groupBy { it.addedBy.id }

            val userInfoMap = tracksByUsers.keys.map { spotifyId ->
                val userInfo = userInfoDao.getUserInfo(spotifyId)
                spotifyId to if (userInfo == null) {
                    val displayName = api.getUser(spotifyId).displayName ?: spotifyId
                    val fetchedUserInfo = UserInfoDto(
                        spotifyId = spotifyId,
                        name = displayName
                    )
                    userInfoDao.setUserInfo(fetchedUserInfo)
                    fetchedUserInfo
                } else {
                    userInfo
                }
            }.toMap()

            val trackByUsersString = if (newItems.size > 9) {
                tracksByUsers.entries.joinToString(separator = "\n") {
                    val username = userInfoMap[it.key]?.name ?: it.key
                    "${it.value.size} tracks by $username"
                }
            } else {
                tracksByUsers.entries.joinToString(separator = "\n") {
                    val tracksString = it.value.joinToString(separator = "\n") { track ->
                        "[${track.artistName} - ${track.trackName}](https://open.spotify.com/track/${track.trackId})"
                    }
                    val username = userInfoMap[it.key]?.name ?: it.key
                    "User $username added:\n$tracksString"
                }
            }
            val message = "New tracks were added to [playlist]($playlistUrl):\n$trackByUsersString"
            tgBot.sendMessage(
                chatId = chatId,
                text = message,
                parseMode = ParseMode.MARKDOWN,
                disableWebPagePreview = true
            )
        }
    }

    timer.scheduleAtFixedRate(timerTask, 3000, cfg.spotifyPlaylistPollingRate.toMillis())

    tgBot.startPolling()
}