import com.wrapper.spotify.model_objects.specification.Playlist
import java.sql.Connection
import java.sql.Timestamp
import java.time.Instant

class ChatPlaylistDao(private val dbConnection: Connection) {

    private companion object Table {
        const val tableName = "chat_playlist"
        const val chatId = "chat_id"
        const val playlistId = "playlist_id"
        const val added = "added"
        const val addedBy = "added_by"
    }

    fun getPlaylist(chatId: Long): ChatPlaylistDto? {
        val statement = dbConnection.prepareStatement("select * from ${Table.tableName} where ${Table.chatId} = ?")
        statement.setLong(1, chatId)
        val result = statement.executeQuery()
        return if (result.next()) {
            ChatPlaylistDto(
                chatId = chatId,
                playlistId = result.getString(playlistId),
                added = result.getTimestamp(added),
                addedBy = result.getLong(addedBy)
            )
        } else {
            null
        }
    }

    fun getAllPlaylists(): Collection<ChatPlaylistDto> {
        val statement = dbConnection.prepareStatement("select * from ${Table.tableName}")
        val result = statement.executeQuery()
        val list = mutableListOf<ChatPlaylistDto>()

        while (result.next()) {
            list.add(
                ChatPlaylistDto(
                    chatId = result.getLong(chatId),
                    playlistId = result.getString(playlistId),
                    added = result.getTimestamp(added),
                    addedBy = result.getLong(addedBy)
                )
            )
        }

        return list
    }

    fun link(chatId: Long, playlistId: String, linkedByUser: Long) {
        val statement = dbConnection.prepareStatement(
            "insert or replace into ${Table.tableName} (${Table.chatId}, ${Table.playlistId}, " +
                    "${Table.addedBy}, ${Table.added}) values (?, ?, ?, ?)"
        )
        statement.setLong(1, chatId)
        statement.setString(2, playlistId)
        statement.setLong(3, linkedByUser)
        statement.setTimestamp(4, Timestamp.from(Instant.now()))
        statement.execute()
    }

    fun unlink(chatId: Long) {
        val statement = dbConnection.prepareStatement("delete from $tableName where $chatId = ?")
        statement.setLong(1, chatId)
        statement.execute()
    }
}