import java.util.*

data class ChatPlaylistDto(
    val chatId: Long,
    val playlistId: String,
    val added: Date,
    val addedBy: Long
)