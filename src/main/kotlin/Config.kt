import java.io.File
import java.time.Duration
import java.util.*

class Config(path: String) {
    private val cfgFile = Properties()
    init {
        cfgFile.load(File(path).inputStream())
    }

    val tgToken: String = cfgFile.getProperty("tg_token")
    val spotifyClientId: String = cfgFile.getProperty("spotify_client_id")
    val spotifySecret: String = cfgFile.getProperty("spotify_secret")
    val dbPath: String = cfgFile.getProperty("db_path")
    val httpServerPort: Int = cfgFile.getProperty("http_server_port").toInt()
    val redirectUrl: String = cfgFile.getProperty("redirect_url")
    val spotifyPlaylistPollingRate: Duration = Duration.parse(cfgFile.getProperty("playlist_polling_rate"))
}