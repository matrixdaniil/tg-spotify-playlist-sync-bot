import java.sql.Connection
import java.sql.Timestamp
import java.util.Date

class PlaylistInfoDao(private val dbConnection: Connection) {

    companion object Table {
        const val tableName = "playlist_tmp_info"
        const val playlistId = "playlist_id"
        const val lastModification = "last_modification"
        const val lastOffset = "last_offset"
    }

    fun find(playlistId: String): PlaylistInfoDto? {
        val statement = dbConnection.prepareStatement(
            "select * from ${Table.tableName} where ${Table.playlistId} = ?"
        )
        statement.setString(1, playlistId)
        val result = statement.executeQuery()
        return if (result.next()) {
            PlaylistInfoDto(
                playlistId = playlistId,
                lastPlaylistModification = Date.from(result.getTimestamp(Table.lastModification).toInstant()),
                lastOffset = result.getInt(Table.lastOffset)
            )
        } else {
            null
        }
    }
    
    fun set(playlistInfo: PlaylistInfoDto) {
        val statement = dbConnection.prepareStatement(
            "insert or replace into ${Table.tableName} (${Table.playlistId}, ${Table.lastModification}, " +
                    "${Table.lastOffset}) values (?, ?, ?)"
        )
        statement.setString(1, playlistInfo.playlistId)
        statement.setTimestamp(2, Timestamp.from(playlistInfo.lastPlaylistModification.toInstant()))
        statement.setInt(3, playlistInfo.lastOffset)
        statement.executeUpdate()
    }
}