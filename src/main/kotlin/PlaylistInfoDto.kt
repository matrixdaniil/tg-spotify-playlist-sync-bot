import java.util.*

data class PlaylistInfoDto(
    val playlistId: String,
    val lastOffset: Int,
    val lastPlaylistModification: Date
)