import java.util.*

data class PlaylistItemDto(
    val addedAt: Date,
    val addedBy: SimplifiedUserDto,
    val trackName: String,
    val artistName: String,
    val trackId: String,
    val trackDuration: Int
)

//TODO remove
data class SimplifiedUserDto(
    val name: String,
    val id: String
)