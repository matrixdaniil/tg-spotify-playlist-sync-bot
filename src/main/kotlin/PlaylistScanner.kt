import com.wrapper.spotify.model_objects.specification.PlaylistTrack
import com.wrapper.spotify.model_objects.specification.Track
import java.util.*

class PlaylistScanner(
    private val playlistInfoDao: PlaylistInfoDao,
    private val api: SpotifyApi,
    private val playlistId: String
) {
    private companion object {
        const val limit = 100
        const val gap = 10
    }

    fun scan(): List<PlaylistItemDto> {
        val playlistInfo = playlistInfoDao.find(playlistId)

        val initialOffset = playlistInfo?.lastOffset ?: 0
        val lastModification = playlistInfo?.lastPlaylistModification ?: Date(0)

        var responseDown = requestItems(initialOffset)
        var responseUp = responseDown
        var currentOffset = initialOffset
        val newTracks = mutableListOf<PlaylistItemDto>()
        newTracks.addAll(scanForNewTracks(responseDown.items, lastModification))

        while (responseDown.total > currentOffset + limit) {
            currentOffset += limit
            responseDown = requestItems(currentOffset)
            newTracks.addAll(scanForNewTracks(responseDown.items, lastModification))
        }

        var lastOffset = currentOffset + responseDown.items.size - gap
        lastOffset = if (lastOffset > 0) lastOffset else 0
        currentOffset = initialOffset
        while (currentOffset > 0 && responseUp.items.firstOrNull()?.addedAt?.after(lastModification) == true) {
            currentOffset = if (currentOffset > limit) currentOffset - limit else 0
            responseUp = requestItems(currentOffset)
            newTracks.addAll(scanForNewTracks(responseDown.items, lastModification))
        }
        
        if (newTracks.size > 0 || lastOffset != initialOffset) {
            playlistInfoDao.set(
                PlaylistInfoDto(
                    playlistId = playlistId,
                    lastOffset = lastOffset,
                    lastPlaylistModification = newTracks.map { it.addedAt }.maxByOrNull { it.time }!!
                )
            )
        }
        
        return newTracks
    }
    
    fun scanEnd() {
        var result = api.getPlaylistItems(playlistId, fields = "total", offset = 0, limit = 1)
        var offset = result.total - 10
        offset = if (offset < 0) 0 else offset
        result = requestItems(offset)

        playlistInfoDao.set(
            PlaylistInfoDto(
                playlistId = playlistId,
                lastOffset = offset,
                lastPlaylistModification = result.items.map { it.addedAt }.maxByOrNull { it.time } ?: Date(0)
            )
        )
    }

    private fun requestItems(offset: Int) =
        api.getPlaylistItems(
            playlistId = playlistId,
            //TODO in that case track items are null, wtf
            //fields = "total, items(added_at, added_by(id, display_name), track(id, name, duration_ms, artists.name))",
            fields = "total, items(added_at, added_by(id, display_name), track)",
            offset = offset
        )

    private fun scanForNewTracks(items: Array<PlaylistTrack>, latestTrack: Date) =
        items.filter { item -> item.addedAt.after(latestTrack) }
            .map { item ->
                val track = item.track as Track
                val user = SimplifiedUserDto(id = item.addedBy.id, name = item.addedBy.displayName ?: item.addedBy.id)
                PlaylistItemDto(
                    addedAt = item.addedAt,
                    addedBy = user,
                    trackName = track.name,
                    trackDuration = track.durationMs,
                    trackId = track.id,
                    artistName = track.artists.joinToString(separator = ", ") { artist -> artist.name }
                )
            }
}