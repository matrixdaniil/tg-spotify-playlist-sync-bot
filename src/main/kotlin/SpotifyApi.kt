import com.neovisionaries.i18n.CountryCode
import com.wrapper.spotify.exceptions.detailed.UnauthorizedException
import org.slf4j.LoggerFactory
import java.time.Instant
import com.wrapper.spotify.SpotifyApi as SpotifyLibApi

class SpotifyApi(private var api: SpotifyLibApi, private val updater: TokenInfoUpdater) {
    private val logger = LoggerFactory.getLogger(SpotifyApi::class.java)

    fun tryAccessPlaylist(playlistId: String) {
        synchronized(this) {
            try {
                api.getPlaylist(playlistId).fields("id")
            } catch (e: UnauthorizedException) {
                refreshToken()
            } catch (e: Throwable) {
                logger.error("tryAccess", e)
                throw e
            }
        }
    }

    fun getPlaylistItems(playlistId: String, fields: String, offset: Int, limit: Int = 100) =
        synchronized(this) {
            try {
                api.getPlaylistsItems(playlistId)
                    .fields(fields)
                    .offset(offset)
                    .limit(limit)
                    .build()
                    .execute()
            } catch (e: UnauthorizedException) {
                refreshToken()
                //TODO project reactor help me!
                //just one retry
                api.getPlaylistsItems(playlistId)
                    .fields(fields)
                    .offset(offset)
                    .limit(100)
                    .build()
                    .execute()
            } catch (e: Throwable) {
                logger.error("getPlaylistItems", e)
                throw e
            }
        }
    
    fun getUser(userId: String) =
        api.getUsersProfile(userId)
            .build()
            .execute()
    
    private fun refreshToken() {
        val result = api.authorizationCodeRefresh().build().execute()
        api = updater.newToken(
            result.accessToken,
            result.refreshToken ?: api.refreshToken,
            Instant.now().plusMillis(result.expiresIn.toLong())
        )
    }
}