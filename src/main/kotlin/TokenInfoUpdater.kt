import com.wrapper.spotify.SpotifyApi as SpotifyLibApi
import java.time.Instant


interface TokenInfoUpdater {
    fun newToken(accesToken: String, refreshToken: String, expiresOn: Instant): SpotifyLibApi
}