import java.sql.Connection
import java.sql.Timestamp
import java.time.Instant

class UserCredentialsDao(private val dbConnection: Connection) {
    data class UserCredentials(
            val userId: Long,
            val accessToken: String,
            val refreshToken: String,
            val expiresOn: Instant
    )
    
    private companion object {
        const val userId = "user_id"
        const val accessToken = "access_token"
        const val refreshToken = "refresh_token"
        const val expiresOn = "expires_on"
        const val tableName = "user_credentials"
    }
    
    fun getUserCreds(userId: Long): UserCredentials? {
        val statement = dbConnection.prepareStatement("select * from user_credentials where user_id = ?")
        statement.setLong(1, userId)
        val result = statement.executeQuery()
        return if (result.next()) {
            UserCredentials(
                    userId = userId,
                    accessToken = result.getString(accessToken),
                    refreshToken = result.getString(refreshToken),
                    expiresOn = result.getTimestamp(expiresOn).toInstant()
            )
        } else {
            null
        }
    }
    
    fun store(creds: UserCredentials) {
        val statement = dbConnection.prepareStatement(
                "insert or replace into $tableName ($userId, $accessToken, $refreshToken, $expiresOn) values (?, ?, ?, ?)"
        )

        statement.setLong(1, creds.userId)
        statement.setString(2, creds.accessToken)
        statement.setString(3, creds.refreshToken)
        statement.setTimestamp(4, Timestamp.from(creds.expiresOn))

        statement.executeUpdate()
    }
}