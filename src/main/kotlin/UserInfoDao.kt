import java.sql.Connection

class UserInfoDao(private val dbConnection: Connection) {

    private companion object Table {
        const val tableName = "user_info"
        const val spotifyId = "spotify_id"
        const val tgId = "tg_id"
        const val name = "name"
    }

    fun setUserInfo(info: UserInfoDto) {
        val statement = dbConnection.prepareStatement(
            "insert or replace into $tableName ($spotifyId, $tgId, $name) values (?, ?, ?)"
        )
        statement.setString(1, info.spotifyId)
        info.tgId?.let { statement.setLong(2, it) }
        statement.setString(3, info.name)
        statement.executeUpdate()
    }
    
    fun getUserInfo(spotifyUserId: String): UserInfoDto? {
        val statement = dbConnection.prepareStatement(
            "select * from $tableName where $spotifyId = ?"
        )
        statement.setString(1, spotifyUserId)
        val result = statement.executeQuery()
        return if (result.next()) {
            UserInfoDto(
                spotifyId = result.getString(spotifyId),
                tgId = result.getLong(tgId),
                name = result.getString(name)
            )
        } else {
            null
        }
    }
}