data class UserInfoDto(
    val spotifyId: String,
    val tgId: Long? = null,
    val name: String
)