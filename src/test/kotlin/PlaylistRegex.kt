import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PlaylistRegex {
    @Test
    fun testA() {
        val playlistRe = Regex("^https?://open\\.spotify\\.com/playlist/([a-zA-Z0-9\\-_]+).*$")
        val source = "https://open.spotify.com/playlist/3jc390nh8uN6NkQzxirC1e?si=m6yUC-p_SA2DeTlkaif9bw"
        val result = playlistRe.find(source)?.groups?.get(1)?.value

        Assertions.assertEquals(
            "3jc390nh8uN6NkQzxirC1e",
            result
        )
    }
}